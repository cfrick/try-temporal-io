package net.ofnir.trytemporal

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import io.temporal.activity.ActivityOptions
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowOptions
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.WorkerFactory
import io.temporal.workflow.Workflow
import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod

import java.time.Duration

class HelloWorldWorker {
    static void main(String... args) {
        def service = WorkflowServiceStubs.newLocalServiceStubs()
        def client = WorkflowClient.newInstance(service)
        def factory = WorkerFactory.newInstance(client)
        def worker = factory.newWorker(HelloWorldWorkflow.HELLO_WORLD_TASK_QUEUE)
        worker.registerWorkflowImplementationTypes(HelloWorldWorkflowImpl)
        worker.registerActivitiesImplementations(new FormatImpl())
        factory.start()
    }
}

class HelloWorldTrigger {
    static void main(String... args) {
        def service = WorkflowServiceStubs.newLocalServiceStubs()
        def client = WorkflowClient.newInstance(service)
        def options = WorkflowOptions.newBuilder()
            .setTaskQueue(HelloWorldWorkflow.HELLO_WORLD_TASK_QUEUE)
            .build()
        def workflow = client.newWorkflowStub(HelloWorldWorkflow, options)
        println workflow.getGreeting("Alice")
    }
}

@ActivityInterface
interface Format {
    @ActivityMethod
    String composeGreeting(String name)
}

class FormatImpl implements Format {
    @Override
    String composeGreeting(String name) {
        return "Hello ${name ?: "World"}"
    }
}


@WorkflowInterface
interface HelloWorldWorkflow {

    public static final HELLO_WORLD_TASK_QUEUE = "HELLO_WORLD_TASK_QUEUE"

    @WorkflowMethod
    String getGreeting(String name)
}

class HelloWorldWorkflowImpl implements HelloWorldWorkflow {

    ActivityOptions options = ActivityOptions.newBuilder()
            .setScheduleToCloseTimeout(Duration.ofSeconds(2))
            .build()

    private final Format format = Workflow.newActivityStub(Format, options)

    @Override
    String getGreeting(String name) {
        format.composeGreeting(name)
    }
}