package net.ofnir.trytemporal

import groovy.util.logging.Slf4j
import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import io.temporal.activity.ActivityOptions
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowOptions
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.WorkerFactory
import io.temporal.workflow.*

import java.time.Duration
import java.util.random.RandomGenerator

class HelloWorldWorker {
    static void main(String... args) {
        def service = WorkflowServiceStubs.newLocalServiceStubs()
        def client = WorkflowClient.newInstance(service)
        def factory = WorkerFactory.newInstance(client)
        def worker = factory.newWorker(ITripBookingWorkflow.TASK_QUEUE)
        worker.registerWorkflowImplementationTypes(TripBookingWorkflow)
        worker.registerActivitiesImplementations(
                new CarBookingActivities(),
                new HotelBookingActivities(),
                new FlightBookingActivities()
        )
        factory.start()
    }
}

class HelloWorldTrigger {
    static void main(String... args) {
        def service = WorkflowServiceStubs.newLocalServiceStubs()
        def client = WorkflowClient.newInstance(service)
        def options = WorkflowOptions.newBuilder()
                .setTaskQueue(ITripBookingWorkflow.TASK_QUEUE)
                .build()
        def workflow = client.newWorkflowStub(ITripBookingWorkflow, options)
        println workflow.bookTrip(UUID.randomUUID().toString())
    }
}


@WorkflowInterface
interface ITripBookingWorkflow {

    public static final TASK_QUEUE = 'trip-booking'

    @WorkflowMethod
    Collection<UUID> bookTrip(String name)

}


@Slf4j
class TripBookingWorkflow implements ITripBookingWorkflow {

    ActivityOptions options = ActivityOptions.newBuilder()
            .setScheduleToCloseTimeout(Duration.ofSeconds(1))
            .build()

    private final ICarBookingActivities carBookingActivities = Workflow.newActivityStub(ICarBookingActivities, options)
    private final IHotelBookingActivities hotelBookingActivities = Workflow.newActivityStub(IHotelBookingActivities, options)
    private final IFlightBookingActivities flightBookingActivities = Workflow.newActivityStub(IFlightBookingActivities, options)

    @Override
    Collection<UUID> bookTrip(String name) {
        def saga = new Saga(new Saga.Options.Builder().build())
        try {
            def carBookingId = carBookingActivities.bookCar(name)
            saga.addCompensation({ -> carBookingActivities.cancelCar(carBookingId) } as Functions.Proc)
            def hotelBookingId = hotelBookingActivities.bookHotel(name)
            saga.addCompensation({ -> hotelBookingActivities.cancelHotel(hotelBookingId) } as Functions.Proc)
            def flightBookingId = flightBookingActivities.bookFlight(name)
            saga.addCompensation({ -> flightBookingActivities.cancelFlight(flightBookingId) } as Functions.Proc)
            return [
                    carBookingId,
                    hotelBookingId,
                    flightBookingId,
            ]
        }
        catch (Exception e) {
            log.error e.message
            saga.compensate()
            throw e
        }
    }
}


@ActivityInterface
interface ICarBookingActivities {
    @ActivityMethod
    UUID bookCar(String name)

    @ActivityMethod
    void cancelCar(UUID booking)
}

@ActivityInterface
interface IHotelBookingActivities {
    @ActivityMethod
    UUID bookHotel(String name)

    @ActivityMethod
    void cancelHotel(UUID booking)
}

@ActivityInterface
interface IFlightBookingActivities {
    @ActivityMethod
    UUID bookFlight(String name)

    @ActivityMethod
    void cancelFlight(UUID booking)
}


@Slf4j
abstract class LoggingBookAndCancel {

    private def rng = RandomGenerator.getDefault()

    abstract String getKey()
    abstract float getSuccessRate()

    UUID book(String name) {
        def rn = rng.nextFloat()
        log.debug "$key: Dice roll: $rn < $successRate = ${rn < successRate}"
        if (rn < successRate) {
            def bookingId = UUID.randomUUID()
            log.info "$key: booked for $name: $bookingId"
            return bookingId
        } else {
            throw new RuntimeException("$key: failure")
        }
    }

    void cancel(UUID bookingId) {
        log.warn "$key: canceled $bookingId"
    }
}

class CarBookingActivities extends LoggingBookAndCancel implements ICarBookingActivities {
    final String key = 'car'
    final float successRate = 0.95f
    @Override UUID bookCar(String name) { super.book(name) }
    @Override void cancelCar(UUID booking) { super.cancel(booking) }
}

class HotelBookingActivities extends LoggingBookAndCancel implements IHotelBookingActivities {
    final String key = 'hotel'
    final float successRate = 0.85f
    @Override UUID bookHotel(String name) { super.book(name) }
    @Override void cancelHotel(UUID booking) { super.cancel(booking) }
}

class FlightBookingActivities extends LoggingBookAndCancel implements IFlightBookingActivities {
    final String key = 'flight'
    final float successRate = 0.75f
    @Override UUID bookFlight(String name) { super.book(name) }
    @Override void cancelFlight(UUID booking) { super.cancel(booking) }
}
