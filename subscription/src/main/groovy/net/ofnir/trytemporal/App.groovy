package net.ofnir.trytemporal

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import io.temporal.activity.ActivityOptions
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowOptions
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.WorkerFactory
import io.temporal.workflow.*

import java.time.Duration

// worker / runners

class SubscriptionWorker {
    static void main(String... args) {
        def service = WorkflowServiceStubs.newLocalServiceStubs()
        def client = WorkflowClient.newInstance(service)
        def factory = WorkerFactory.newInstance(client)
        def worker = factory.newWorker(ISubscriptionWorkflow.TASK_QUEUE)
        worker.registerWorkflowImplementationTypes(SubscriptionWorkflow)
        worker.registerActivitiesImplementations(new SubscriptionActivities())
        factory.start()
    }
}

class SubscriptionTrigger {
    static void main(String... args) {
        def service = WorkflowServiceStubs.newLocalServiceStubs()
        def client = WorkflowClient.newInstance(service)
        def customer = new Customer(
                UUID.randomUUID(),
                'lol@example.com',
                new Subscription(
                        Duration.ofSeconds(15),
                        Duration.ofSeconds(5),
                        5,
                        10.0
                )
        )
        def options = WorkflowOptions.newBuilder()
                .setWorkflowId(customer.id().toString())
                .setTaskQueue(ISubscriptionWorkflow.TASK_QUEUE)
                .build()
        def workflow = client.newWorkflowStub(ISubscriptionWorkflow, options)
        workflow.startTrial(customer)
    }
}

// DTOs

record Customer(
        UUID id,
        String email,
        Subscription subscription
) {}

record Subscription(
        Duration trialPeriod,
        Duration billingPeriod,
        long maxBillingPeriods,
        BigDecimal initialBillingPeriodCharge
) {}

// Activities

@ActivityInterface
interface ISubscriptionActivities {

    @ActivityMethod
    void sendWelcomeEmail(Customer customer)

    @ActivityMethod
    void sendCancellationEmailDuringTrialPeriod(Customer customer)

    @ActivityMethod
    void chargeCustomerForBillingPeriod(Customer customer, BigDecimal chargeAmount)

    @ActivityMethod
    void sendCancellationEmailDuringActiveSubscription(Customer customer)

    @ActivityMethod
    void sendSubscriptionOverEmail(Customer customer)
}

class SubscriptionActivities implements ISubscriptionActivities {

    void sendWelcomeEmail(Customer customer) {
        println "sendWelcomeEmail($customer)"
    }

    void sendCancellationEmailDuringTrialPeriod(Customer customer) {
        println "sendCancellationEmailDuringTrialPeriod($customer)"
    }

    void chargeCustomerForBillingPeriod(Customer customer, BigDecimal chargeAmount) {
        println "chargeCustomerForBillingPeriod($customer, $chargeAmount)"
    }

    void sendCancellationEmailDuringActiveSubscription(Customer customer) {
        println "sendCancellationEmailDuringActiveSubscription($customer)"
    }

    void sendSubscriptionOverEmail(Customer customer) {
        println "sendSubscriptionOverEmail($customer)"
    }

}

// workflows

@WorkflowInterface
interface ISubscriptionWorkflow {

    public static final TASK_QUEUE = "SUBSCRIPTION_TASK_QUEUE"

    @WorkflowMethod
    void startTrial(Customer customer)

    @SignalMethod(name = "cancelSubscription")
    void cancel()

    @QueryMethod(name = 'billingPeriodCharge')
    BigDecimal getBillingPeriodCharge()

    @QueryMethod(name = 'billingPeriods')
    long getBillingPeriods()

}

class SubscriptionWorkflow implements ISubscriptionWorkflow {

    ActivityOptions options = ActivityOptions.newBuilder()
            .setScheduleToCloseTimeout(Duration.ofSeconds(2))
            .build()

    private final ISubscriptionActivities activities = Workflow.newActivityStub(ISubscriptionActivities, options)

    boolean canceled = false
    BigDecimal billingPeriodCharge = 10.0
    long billingPeriods = 0

    @Override
    void startTrial(Customer customer) {
        activities.sendWelcomeEmail(customer)
        if (Workflow.await(customer.subscription().trialPeriod(), { canceled })) {
            activities.sendCancellationEmailDuringTrialPeriod(customer)
        } else {
            while (true) {
                def maxBillingPeriods = customer.subscription().maxBillingPeriods()
                if (maxBillingPeriods > 0 && billingPeriods > maxBillingPeriods) {
                    break
                }
                activities.chargeCustomerForBillingPeriod(customer, billingPeriodCharge)
                billingPeriods++
                if (Workflow.await(customer.subscription().billingPeriod(), { canceled })) {
                    activities.sendCancellationEmailDuringActiveSubscription(customer)
                    break
                }
            }
            if (!canceled) {
                activities.sendSubscriptionOverEmail(customer)
            }
        }
    }

    @Override
    void cancel() {
        this.canceled = true
    }
}
