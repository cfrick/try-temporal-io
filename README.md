# Try out temporal.io

Following along the docs with Groovy

## Hello-World

https://learn.temporal.io/getting_started/java/hello_world_in_java/

## Subscription

https://learn.temporal.io/tutorials/typescript/subscriptions/

https://github.com/temporalio/subscription-workflow-project-template-typescript/

## Trip booking

https://learn.temporal.io/tutorials/php/booking_saga/
